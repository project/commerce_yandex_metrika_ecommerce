Module implements yandex metrika ecommerce integration

After installation:

1) enable this module,

2) go to settings page (admin/config/commerce_yandex_metrika_ecommerce) and 
enter a goal id for completed checkout;

3) configure block 'Commerce Yandex Metrica Ecommerce Script' for showing at
every page.

Produced by rostechcom.ru.

It will be great if you donate some part of your profit to WebMoney 
R273140314399 or Z419624153348.
