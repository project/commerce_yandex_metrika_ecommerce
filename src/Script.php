<?php

namespace Drupal\commerce_yandex_metrika_ecommerce;

/**
 * Creating script with data for Yandex Metrika.
 */
class Script {

  /**
   * Constructor.
   *
   * @param array $items
   *   Items for Yandex Metrika: <br>
   *    content - item content with data for Yandex Metrika: <br>
   *      type - 'product' or 'order', <br>
   *
   *      for product type: <br>
   *      currency_code - currency code, <br>
   *      action        - 'add' or 'remove' product in/from cart, <br>
   *      id            - product id, <br>
   *      title         - product title, <br>
   *      quantity      - product quantity, <br>
   *      price         - product price without the decimal, <br>
   *      brand         - brands titles from root to term (optional), <br>
   *      category      - categories titles from root to term (optional), <br>
   *
   *      for order type: <br>
   *        currency_code - currency code, <br>
   *        id            - order id, <br>
   *        total         - order total price without the decimal, <br>
   *        products      - order products: <br>
   *          id       - product id, <br>
   *          title    - product title, <br>
   *          quantity - product quantity, <br>
   *          price    - product price without the decimal, <br>
   *          brand         - brands titles from root to term (optional), <br>
   *          category      - categories titles from root to term (optional).
   */
  public function __construct(array $items) {
    $this->items = $items;
  }

  /**
   * Show script.
   */
  public function show() { ?>
    <script type="text/javascript">
      Drupal.behaviors.commerce_yandex_metrika_ecommerce = {
        attach: function() {
          window.dataLayer = window.dataLayer || [];
          let aItems = (<?php echo json_encode($this->getItems())?>);
          for(let i = 0; i < aItems.length; i++) {
            window.dataLayer.push(aItems[i]);
          }
        }
      }
    </script>
  <?php }

  /**
   * Items for Yandex Metrika.
   *
   * Format: <br>
   *  content - item content with data for Yandex Metrika: <br>
   *    type - 'product' or 'order', <br>
   *
   *    for product type: <br>
   *    currency_code - currency code, <br>
   *    action        - 'add' or 'remove' product in/from cart, <br>
   *    id            - product id, <br>
   *    title         - product title, <br>
   *    quantity      - product quantity, <br>
   *    price         - product price without the decimal, <br>
   *    brand         - brands titles from root to term (optional), <br>
   *    category      - categories titles from root to term (optional), <br>
   *
   *    for order type: <br>
   *      currency_code - currency code, <br>
   *      id            - order id, <br>
   *      total         - order total price without the decimal, <br>
   *      products      - order products: <br>
   *        id       - product id, <br>
   *        title    - product title, <br>
   *        quantity - product quantity, <br>
   *        price    - product price without the decimal, <br>
   *        brand         - brands titles from root to term (optional), <br>
   *        category      - categories titles from root to term (optional).
   *
   * @var array
   */
  private $items;

  /**
   * Get items array in Yandex Metrika format.
   *
   * @see https://yandex.ru/support/metrika/data/e-commerce.html
   *
   * @return array
   *   Items in Yandex Metrika format.
   */
  private function getItems() {
    $checkout_goal = variable_get('commerce_yandex_metrika_ecommerce_checkout_complete_goal_id');
    $items = [];
    foreach ($this->items as $item) {
      if ($item->content->type == 'product') {
        $product_one = [
          'id' => $item->content->id,
          'name' => $item->content->title,
          'quantity' => $item->content->quantity,
          'price' => $item->content->price,
        ];
        if (isset($item->content->brand)) {
          $product_one['brand'] = end($item->content->brand);
        }
        if (isset($item->content->category)) {
          $product_one['category'] = implode('/', array_slice($item->content->category, -5));
        }
        $items[] = [
          'ecommerce' => [
            'currencyCode' => $item->content->currency_code,
            $item->content->action => [
              'products' => [$product_one,
              ],
            ],
          ],
        ];
      }
      elseif ($item->content->type == 'order') {
        $products = [];
        foreach ($item->content->products as $product) {
          $product_one = [
            'id' => $product->id,
            'name' => $product->title,
            'quantity' => $product->quantity,
            'price' => $product->price,
          ];
          if (isset($product->brand)) {
            $product_one['brand'] = end($product->brand);
          }
          if (isset($product->category)) {
            $product_one['category'] = implode('/', array_slice($product->category, -5));
          }
          $products[] = $product_one;
        }
        $order_item = [
          'ecommerce' => [
            'currencyCode' => $item->content->currency_code,
            'purchase' => [
              'actionField' => [
                'id' => $item->content->id,
                'revenue' => $item->content->total,
              ],
              'products' => $products,
            ],
          ],
        ];
        if ($checkout_goal) {
          $order_item['ecommerce']['purchase']['actionField']['goal_id'] = $checkout_goal;
        }
        $items[] = $order_item;
      }
    }
    return $items;
  }

}
