<?php

namespace Drupal\commerce_yandex_metrika_ecommerce;

/**
 * Saving data for exporting to Yandex Metrika.
 */
class Storage {

  /**
   * Save line item.
   *
   * @param \EntityMetadataWrapper $line_item_wrapper
   *   Commerce line item wrapper.
   * @param int $quantity
   *   Quantity (>0 - products were added, <0 - products were removed).
   *
   * @global \stdClass $user
   *   An object representing the user currently visiting the site.
   */
  public function addLineItem(\EntityMetadataWrapper $line_item_wrapper, $quantity) {
    global $user;
    $order = $line_item_wrapper->order->value();
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    $line_item_id = $line_item_wrapper->getIdentifier();
    if ($quantity <> 0 && isset($line_item_wrapper->commerce_product) &&
       $order_wrapper->uid->value() == $user->uid &&
       !isset(static::$lineItems[$line_item_id])) {
      static::$lineItems[$line_item_id] = $line_item_id;
      if ($quantity > 0) {
        $action = 'add';
      }
      else {
        $action = 'remove';
      }
      $product = $line_item_wrapper->commerce_product->value();
      $product_wrapper = entity_metadata_wrapper('commerce_product', $product);
      $data = [
        'type' => 'product',
        'action' => $action,
        'id' => $product_wrapper->product_id->value(),
        'title' => $product_wrapper->title->value(),
        'quantity' => abs(round($quantity)),
        'price' => $line_item_wrapper->commerce_unit_price->value()['amount'] / 100,
        'currency_code' => $order_wrapper->commerce_order_total->currency_code->value(),
      ];
      $this->attachProductTaxonomies($data, $product_wrapper->product_id->value());
      $now = new \DateTime();
      db_insert('commerce_yandex_metrika_ecommerce')
        ->fields([
          'moment' => $now->getTimestamp(),
          'content' => json_encode($data),
          'session_id' => session_id(),
        ])
        ->execute();
      $now->sub(new \DateInterval('P1D'));
      db_delete('commerce_yandex_metrika_ecommerce')
        ->condition('moment', $now->getTimestamp(), '<=')
        ->execute();
    }
  }

  /**
   * Save order.
   *
   * @param \stdClass $order
   *   Commerce order object.
   *
   * @global \stdClass $user
   *   An object representing the user currently visiting the site.
   */
  public function addOrder(\stdClass $order) {
    global $user;
    $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
    if ($order_wrapper->uid->value() == $user->uid) {
      $products = [];
      foreach ($order_wrapper->commerce_line_items as $line_item) {
        if (isset($line_item->commerce_product)) {
          $product = entity_metadata_wrapper('commerce_product', $line_item->commerce_product->value());
          $product_one = [
            'id' => $product->product_id->value(),
            'title' => $product->title->value(),
            'quantity' => round($line_item->quantity->value()),
            'price' => $line_item->commerce_unit_price->value()['amount'] / 100,
          ];
          $this->attachProductTaxonomies($product_one, $product->product_id->value());
          $products[] = $product_one;
        }
      }
      $data = [
        'type' => 'order',
        'id' => $order_wrapper->order_id->value(),
        'total' => $order_wrapper->commerce_order_total->value()['amount'] / 100,
        'products' => $products,
        'currency_code' => $order_wrapper->commerce_order_total->currency_code->value(),
      ];
      $now = new \DateTime();
      db_insert('commerce_yandex_metrika_ecommerce')
        ->fields([
          'moment' => $now->getTimestamp(),
          'content' => json_encode($data),
          'session_id' => session_id(),
        ])
        ->execute();
      $now->sub(new \DateInterval('P1D'));
      db_delete('commerce_yandex_metrika_ecommerce')
        ->condition('moment', $now->getTimestamp(), '<=')
        ->execute();
    }
  }

  /**
   * Get items from current session.
   *
   * @param bool $clear
   *   Delete items.
   *
   * @return array
   *   Items for Yandex Metrika: <br>
   *    content - item content with data for Yandex Metrika: <br>
   *      type - 'product' or 'order', <br>
   *
   *      for product type: <br>
   *      currency_code - currency code, <br>
   *      action        - 'add' or 'remove' product in/from cart, <br>
   *      id            - product id, <br>
   *      title         - product title, <br>
   *      quantity      - product quantity, <br>
   *      price         - product price without the decimal, <br>
   *
   *      for order type: <br>
   *        id       - order id, <br>
   *        total    - order total price without the decimal, <br>
   *        products - order products: <br>
   *          id       - product id, <br>
   *          title    - product title, <br>
   *          quantity - product quantity, <br>
   *          price    - product price without the decimal.
   */
  public function getItems($clear = TRUE) {
    $ids = db_select('commerce_yandex_metrika_ecommerce', 'c')
      ->fields('c', ['id'])
      ->condition('session_id', session_id())
      ->execute()
      ->fetchAll();
    $result = [];
    foreach ($ids as $id) {
      $transaction = db_transaction();
      $items = db_select('commerce_yandex_metrika_ecommerce', 'c')
        ->fields('c', ['moment', 'content', 'session_id'])
        ->condition('id', $id->id)
        ->forUpdate(TRUE)
        ->execute()
        ->fetchAll();
      if ($clear) {
        db_delete('commerce_yandex_metrika_ecommerce')
          ->condition('id', $id->id)
          ->execute();
      }
      foreach ($items as $item) {
        $item->content = json_decode($item->content);
        $result[] = $item;
      }
      unset($transaction);
    }
    return $result;
  }

  /**
   * Processed line_items ids for including them just once.
   *
   * @var int[]
   */
  private static $lineItems = [];

  /**
   * Attach additional taxonomies information to product data array.
   *
   * @param array $data
   *   Product data array.
   * @param int $product_id
   *   Product id.
   */
  private function attachProductTaxonomies(array &$data, $product_id) {
    $node = $this->getProductNode($product_id);
    if ($node) {
      $node_wrapper = entity_metadata_wrapper('node', $node);
      $category_field = variable_get('commerce_yandex_metrika_ecommerce_category_field');
      if ($category_field) {
        $data['category'] = $this->getTaxonomyHierarchy($node_wrapper, $category_field);
      }
      $brand_field = variable_get('commerce_yandex_metrika_ecommerce_brand_field');
      if ($brand_field) {
        $data['brand'] = $this->getTaxonomyHierarchy($node_wrapper, $brand_field);
      }
    }
  }

  /**
   * Get product node.
   *
   * @param int $product_id
   *   Product id.
   *
   * @return mixed
   *   Node object or null if node was not found.
   */
  private function getProductNode($product_id) {
    $field = variable_get('commerce_yandex_metrika_ecommerce_product_ref');
    $node = NULL;
    if ($field) {
      $query = new \EntityFieldQuery();
      $query->entityCondition('entity_type', 'node', '=')
        ->fieldCondition($field, 'product_id', $product_id, '=')
        ->range(0, 1);

      if ($result = $query->execute()) {
        $node_id = array_shift(array_keys($result['node']));
        $node = node_load($node_id);
      }
    }

    return $node;
  }

  /**
   * Get node taxonomy hierarchy from field.
   *
   * @param \EntityMetadataWrapper $node_wrapper
   *   Node wrapper.
   * @param string $field
   *   Taxonomy node field.
   *
   * @return array
   *   Full terms hierarchy from root to term.
   */
  private function getTaxonomyHierarchy(\EntityMetadataWrapper $node_wrapper, $field) {
    $result = [];
    if (isset($node_wrapper->$field)) {
      $node_field = $node_wrapper->$field;
      $term = $node_field->value();
      if ($term) {
        $term_wrapper = entity_metadata_wrapper('taxonomy_term', $term);
        $parents = $term_wrapper->parents_all->value();
        foreach ($parents as $parent) {
          array_unshift($result, $parent->name);
        }
      }
    }
    return $result;
  }

}
